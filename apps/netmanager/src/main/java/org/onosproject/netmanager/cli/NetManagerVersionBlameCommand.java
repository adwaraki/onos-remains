/*
 * Copyright 2014 Open Networking Laboratory
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.onosproject.netmanager.cli;

import org.apache.karaf.shell.commands.Argument;
import org.apache.karaf.shell.commands.Command;
import org.onosproject.cli.AbstractShellCommand;
import org.onosproject.core.UnavailableIdException;
import org.onosproject.netmanager.impl.ColorCodes;
import org.onosproject.netmanager.impl.DatastoreQueryResponse;
import org.onosproject.netmanager.impl.NetManagerService;
import org.onosproject.netmanager.impl.VersionData;

import java.util.Vector;

/**
 * Command to describe a commit in detail
 */
@Command(scope = "onos", name = "nm-blame",
        description = "Per-commit author tracking")
public class NetManagerVersionBlameCommand extends AbstractShellCommand {

    @Argument(index = 0, name = "flow-id", description = "flow-id of rule to be investigated",
            required = true, multiValued = false)
    long id;

    DatastoreQueryResponse cmdResponse;
    Vector<VersionData> flowVersions;

    @Override
    protected void execute() {

        NetManagerService vcService = AbstractShellCommand.get(NetManagerService.class);
        cmdResponse = vcService.netManagerVersionBlame(id);
        int resCode = cmdResponse.getResponseCode();
        if (resCode == 99) {
            System.out.println(cmdResponse.getResponseMessage());
            System.out.println(ColorCodes.ANSI_RED + "Error fetching commits for flow, " +
                    "check log for details");
        } else {
            System.out.println(cmdResponse.getResponseMessage());
            flowVersions = cmdResponse.getResultVector();
            String dateFormat = "EEE, MMM dd HH:mm:ss yyyy ";
            // display information accordingly
            for (VersionData commitEntry : flowVersions) {

                System.out.println(ColorCodes.ANSI_YELLOW + "commit "
                        + commitEntry.getCommitId().stringValue()
                        + ColorCodes.ANSI_RESET);
                System.out.format("%-15s : %s %n",
                        "Version", commitEntry.getVersion());
                System.out.format("%-15s : %s %n",
                        "Author-id", commitEntry.getAuthorId());
                System.out.format("%-15s : %s %n",
                        "Author name", commitEntry.getAuthorName());
                System.out.format("%-15s : %s %n",
                        "Date:", commitEntry.getDateTime().toString(dateFormat));
                System.out.format("%-15s : %s %n",
                        "Active version", commitEntry.isActiveVersion());
                System.out.println("\n");
            }
        }
    }
}