package org.onosproject.netmanager.impl;

import com.google.common.base.Objects;

/**
 * Created by Abhishek on 8/12/15.
 */
public class CommitId {

    private final byte[] commitId;
    private final String commitHexRep;
    private boolean isGroupCommit;

    private CommitId(byte[] id) {
        this.commitId = id;
        this.commitHexRep = convertToHex(commitId);
    }

    public static CommitId valueOf(byte[] id) {
        return new CommitId(id);
    }

    public byte[] value() {
        return commitId;
    }

    public String stringValue() {
        return commitHexRep;
    }

    public boolean isGroupCommit() {
        return this.isGroupCommit;
    }

    public void setIsGroupCommit(boolean isGroupCommit) {
        this.isGroupCommit = isGroupCommit;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (obj.getClass()  == this.getClass()) {
            CommitId that = (CommitId) obj;
            return Objects.equal(this.commitId, that.commitId);
        }
        return false;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(this.commitId);
    }

    /**
     * Internal method to convert the SHA1 hash byte array into a 40
     * character hex value
     * @param data hashed byte array
     * @return 40 character hex string
     */
    private static String convertToHex(byte[] data) {
        StringBuilder buf = new StringBuilder();
        for (byte aData : data) {
            int halfbyte = (aData >>> 4) & 0x0F;
            int two_halfs = 0;
            do {
                if ((0 <= halfbyte) && (halfbyte <= 9))
                    buf.append((char) ('0' + halfbyte));
                else
                    buf.append((char) ('a' + (halfbyte - 10)));
                halfbyte = aData & 0x0F;
            } while (two_halfs++ < 1);
        }
        return buf.toString();
    }
}
