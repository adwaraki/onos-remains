/*
 * Copyright 2014 Open Networking Laboratory
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.onosproject.netmanager.cli;

import org.apache.karaf.shell.commands.Argument;
import org.apache.karaf.shell.commands.Command;
import org.onosproject.cli.AbstractShellCommand;
import org.onosproject.core.UnavailableIdException;
import org.onosproject.netmanager.impl.ColorCodes;
import org.onosproject.netmanager.impl.DatastoreQueryResponse;
import org.onosproject.netmanager.impl.NetManagerService;
import org.onosproject.netmanager.impl.VersionData;

import java.util.Map;
import java.util.Vector;

/**
 * Command to describe a commit in detail
 */
@Command(scope = "onos", name = "nm-describe",
        description = "Describes a commit in detail")
public class NetManagerCommitShowCommand extends AbstractShellCommand {

    @Argument(index = 0, name = "commitId", description = "ID of commit to be described",
            required = true, multiValued = false)
    String id = null;

    DatastoreQueryResponse cmdResponse;
    Vector<VersionData> commitVersionVector;

    @Override
    protected void execute() {

        // get a reference to the VC Service
        NetManagerService vcService = AbstractShellCommand.get(NetManagerService.class);
        // retrieve the commit map from the table backed datastore
        if(id == null) {
            log.error("Need commit id to retrieve info");
            throw new UnavailableIdException("Commit id not present");
        }
        else {
            cmdResponse = vcService.netManagerCommitShow(id);
            int resCode = cmdResponse.getResponseCode();
            if (resCode == 99) {
                System.out.println(cmdResponse.getResponseMessage());
                System.out.println(ColorCodes.ANSI_RED + "Error fetching commit, " +
                        "check log for details");
            } else {
                commitVersionVector = cmdResponse.getResultVector();
                String dateFormat = "EEE, MMM dd HH:mm:ss yyyy ";
                // display information accordingly
                for (VersionData commitEntry : commitVersionVector) {

                    // VersionData versionInfo = commitEntry.getValue().lastElement();
                    // log.info("to-string of version data : " + versionInfo.toString());
                    // log.info("Number of flows in commit list : "
                    //        + commitEntry.getValue().size());

                    System.out.println(ColorCodes.ANSI_YELLOW + "commit "
                            + commitEntry.getCommitId().stringValue()
                            + ColorCodes.ANSI_RESET);
                    System.out.format("%-15s : %s %n",
                            "Author-id", commitEntry.getAuthorId());
                    System.out.format("%-15s : %s %n",
                            "Author name", commitEntry.getAuthorName());
                    System.out.format("%-15s : %s %n",
                            "Date:", commitEntry.getDateTime().toString(dateFormat));
                    System.out.format("%-15s : %s %n",
                            "Flow-id", commitEntry.getFlowId());
                    System.out.format("%-15s : %s %n",
                            "Active version", commitEntry.isActiveVersion());
                    System.out.format("%-15s : %s %n",
                            "Flow priority", commitEntry.getPriority());
                    System.out.format("%-15s : %s %n",
                            "Device-id", commitEntry.getDeviceId());
                    System.out.format("%-15s : %s %n",
                            "Table-id", commitEntry.getTableId());
                    System.out.format("%-15s : %s %n",
                            "Match-fields",
                            (commitEntry.getRuleSelector() == null ? "" :
                                    commitEntry.getRuleSelector().criteria()));
                    System.out.format("%-15s : %s %n",
                            "Action-set",
                            (commitEntry.getRuleTreatment() == null ? "" :
                                    commitEntry.getRuleTreatment().allInstructions()));
                    System.out.println("\n");
                }
            }
        }
    }
}