/*
 * Copyright 2014 Open Networking Laboratory
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.onosproject.netmanager.cli;

import org.apache.karaf.shell.commands.Command;
import org.apache.karaf.shell.commands.Option;
import org.onosproject.cli.AbstractShellCommand;
import org.onosproject.netmanager.impl.ColorCodes;
import org.onosproject.netmanager.impl.DatastoreQueryResponse;
import org.onosproject.netmanager.impl.NetManagerService;
import org.onosproject.netmanager.impl.VersionData;

import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

/**
 * Command that lists all the flow commits made to the datastore
 * up until the current wallclock time
 */
@Command(scope = "onos", name = "nm-find",
        description = "Extracts commits from the repository based on specified filters")
public class NetManagerFindCommand extends AbstractShellCommand {

    @Option(name = "-a", aliases = "--app-id", description = "Application ID",
            required = false, multiValued = false)
    short appId = -1;

    @Option(name = "-f", aliases = "--flow-id", description = "Flow ID",
            required = false, multiValued = false)
    long flowId = 0;

    @Option(name = "-t", aliases = "--table-id", description = "Table ID",
            required = false, multiValued = false)
    int tableId = -1;

    @Option(name = "-c", aliases = "--commit-id", description = "Commit ID",
            required = false, multiValued = false)
    String commitId = null;

    @Option(name = "-d", aliases = "--device-id", description = "Device ID",
            required = false, multiValued = false)
    String deviceUri = null;

    @Option(name = "-sm", aliases = "--src-mac", description = "Source MAC Address",
            required = false, multiValued = false)
    String sMac = null;

    @Option(name = "-y", aliases = "--active", description = "Version status",
            required = false, multiValued = false)
    String activeState = null;

    // data structure to hold the response
    DatastoreQueryResponse cmdResponse;
    private Vector<VersionData> versionDataVector;

    @Override
    protected void execute() {

        // get a reference to the VC Service
        NetManagerService vcService = AbstractShellCommand.get(NetManagerService.class);
        Map<String, Object> params = new HashMap<>();
        params.put("app-id", appId);
        params.put("flow-id", flowId);
        params.put("commit-id", commitId);
        params.put("table-id", tableId);
        params.put("device-id", deviceUri);
        params.put("src-mac", sMac);
        params.put("active", activeState);

        cmdResponse = vcService.netManagerFind(params);
        int resCode = cmdResponse.getResponseCode();
        if(resCode == 99) {
            System.out.println(cmdResponse.getResponseMessage());
            System.out.println(ColorCodes.ANSI_RED + "Error fetching commits, " +
                    "check log for details");
        } else {
            versionDataVector = cmdResponse.getResultVector();
            String dateFormat = "EEE MMM dd HH:mm:ss yyyy ";

            for(VersionData entry : versionDataVector) {
                System.out.println(ColorCodes.ANSI_YELLOW + "commit  "
                        + entry.getCommitId().stringValue()
                        + ColorCodes.ANSI_RESET);
                System.out.format("%-15s : %s %n",
                        "Version", entry.getVersion());
                System.out.format("%-15s : %s %n",
                        "Author-id", entry.getAuthorId());
                System.out.format("%-15s : %s %n",
                        "Author name", entry.getAuthorName());
                System.out.format("%-15s : %s %n",
                        "Date:", entry.getDateTime().toString(dateFormat));
                System.out.format("%-15s : %s %n",
                        "Flow-id", entry.getFlowId());
                System.out.format("%-15s : %s %n",
                        "Active version", entry.isActiveVersion());
                System.out.format("%-15s : %s %n",
                        "Flow priority", entry.getPriority());
                System.out.format("%-15s : %s %n",
                        "Device-id", entry.getDeviceId());
                System.out.format("%-15s : %s %n",
                        "Table-id", entry.getTableId());
                System.out.format("%-15s : %s %n",
                        "Match-fields",
                        (entry.getRuleSelector() == null ? "" :
                                entry.getRuleSelector().criteria()));
                System.out.format("%-15s : %s %n",
                        "Action-set",
                        (entry.getRuleTreatment() == null ? "" :
                                entry.getRuleTreatment().allInstructions()));
                System.out.println("\n");
            }
        }
    }
}