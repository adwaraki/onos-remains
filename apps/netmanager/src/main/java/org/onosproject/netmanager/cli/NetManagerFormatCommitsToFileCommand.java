/*
 * Copyright 2014 Open Networking Laboratory
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.onosproject.netmanager.cli;

import org.apache.karaf.shell.commands.Command;
import org.apache.karaf.shell.commands.Option;
import org.onosproject.cli.AbstractShellCommand;
import org.onosproject.netmanager.impl.ColorCodes;
import org.onosproject.netmanager.impl.DatastoreQueryResponse;
import org.onosproject.netmanager.impl.NetManagerService;
import org.onosproject.netmanager.impl.VersionData;

import com.eclipsesource.json.JsonObject;
import com.eclipsesource.json.WriterConfig;

import java.util.Vector;
import java.io.*;

/**
 * Command that lists all the flow commits made to the datastore
 * up until the current wallclock time
 */
@Command(scope = "onos", name = "nm-print-commits",
        description = "Does a state log dump to file, formatted as JSOM")
public class NetManagerFormatCommitsToFileCommand extends AbstractShellCommand {

    @Option(name = "-o", aliases = "--output-file",
            description = "JSON-formatted output file with extended commit info",
            required = false, multiValued = false)
    String fileName = null;

    DatastoreQueryResponse cmdResponse;
    private Vector<VersionData> versionDataVector;

    @Override
    protected void execute() {

        // get a reference to the VC Service
        NetManagerService vcService = AbstractShellCommand.get(NetManagerService.class);
        cmdResponse = vcService.netManagerStateLog();
        int resCode = cmdResponse.getResponseCode();
        if (resCode == 99) {
            System.out.println(cmdResponse.getResponseMessage());
            System.out.println(ColorCodes.ANSI_RED + "Error fetching commit log, " +
                    "check system log for details");
        } else {
            String dateFormat = "EEE MMM dd HH:mm:ss yyyy ";
            versionDataVector = cmdResponse.getResultVector();
            try {
                Writer jsonWriter = new FileWriter(fileName);
                for (VersionData entry : versionDataVector) {

                    JsonObject entryObject = new JsonObject();
                    entryObject.add("commit", entry.getCommitId().stringValue());
                    entryObject.add("version", entry.getVersion());
                    entryObject.add("author-id", entry.getAuthorId());
                    entryObject.add("author-name", entry.getAuthorName());
                    entryObject.add("date", entry.getDateTime().toString(dateFormat));
                    entryObject.add("flow-id", entry.getFlowId());
                    entryObject.add("active-version", entry.isActiveVersion());
                    entryObject.add("flow-priority", entry.getPriority());
                    entryObject.add("device-id", entry.getDeviceId());
                    entryObject.add("table-id", entry.getTableId());
                    entryObject.add("match-fields", (entry.getRuleSelector() == null ? "" :
                            entry.getRuleSelector().criteria().toString()));
                    entryObject.add("action-set", (entry.getRuleTreatment() == null ? "" :
                            entry.getRuleTreatment().allInstructions().toString()));

                    entryObject.writeTo(jsonWriter, WriterConfig.PRETTY_PRINT);
                    jsonWriter.flush();
                }
                jsonWriter.close();
            } catch (IOException ioException) {
                ioException.printStackTrace();
            }
        }
    }
}