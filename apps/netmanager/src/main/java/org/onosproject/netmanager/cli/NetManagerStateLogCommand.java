/*
 * Copyright 2014 Open Networking Laboratory
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.onosproject.netmanager.cli;

import org.apache.karaf.shell.commands.Command;
import org.onosproject.cli.AbstractShellCommand;
import org.onosproject.netmanager.impl.ColorCodes;
import org.onosproject.netmanager.impl.DatastoreQueryResponse;
import org.onosproject.netmanager.impl.NetManagerService;
import org.onosproject.netmanager.impl.VersionData;

import java.util.Vector;

/**
 * Command that lists all the flow commits made to the datastore
 * up until the current wallclock time
 */
@Command(scope = "onos", name = "nm-log",
        description = "Lists all the commits made to the repository")
public class NetManagerStateLogCommand extends AbstractShellCommand {

    DatastoreQueryResponse cmdResponse;
    private Vector<VersionData> versionDataVector;

    @Override
    protected void execute() {

        // get a reference to the VC Service
        NetManagerService vcService = AbstractShellCommand.get(NetManagerService.class);
        cmdResponse = vcService.netManagerStateLog();
        int resCode = cmdResponse.getResponseCode();
        if (resCode == 99) {
            System.out.println(cmdResponse.getResponseMessage());
            System.out.println(ColorCodes.ANSI_RED + "Error fetching commit log, " +
                    "check system log for details");
        } else {
            String dateFormat = "EEE MMM dd HH:mm:ss yyyy ";
            versionDataVector = cmdResponse.getResultVector();
            for (VersionData entry : versionDataVector) {
                System.out.println(ColorCodes.ANSI_YELLOW + "commit  "
                        + entry.getCommitId().stringValue()
                        + ColorCodes.ANSI_RESET);
                System.out.format("%-15s : %s %n",
                        "Group commit", entry.getCommitId().isGroupCommit());
                System.out.format("%-15s : %s %n",
                        "Author:", entry.getAuthorName());
                System.out.format("%-15s : %s %n",
                        "Referenced-flow", entry.getFlowId());
                System.out.format("%-15s : %s %n",
                        "Date:", entry.getDateTime().toString(dateFormat));
                System.out.println("\n");
            }
        }
    }
}