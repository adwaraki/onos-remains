/*
 * Copyright 2014 Open Networking Laboratory
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.onosproject.netmanager.impl;

import org.onosproject.netmanager.impl.NetManagerService;
import org.onosproject.netmanager.impl.VersionData;

import static com.google.common.base.Preconditions.checkNotNull;

import java.util.Vector;

public class DatastoreQueryResponse {
    /**
     * Data model for the datastore's response
     */
    // TO-DO: List response codes
    private final int responseCode;
    // Some commands might just need to return a TRUE/FALSE
    private final boolean responseFlag;
    private final String responseMessage;
    // The result vector is null in case of an error and when a query does
    // not generate a vector. In those cases, either a POJO is used or the
    // responseMessage holds information.
    private final Vector<VersionData> resultVector;
    // Fall-back response object for any query's response that is not a vector
    private final Object responseObject;

    /**
     * Constructor for the DatastoreQueryResponse class
     * @param resCode result code for the operation
     * @param resMessage corresponding result message
     * @param resVector resulting commit vector if present,
     *                  null otherwise
     * @param resObject generic POJO to hold information if
     *                  resVector is null. resMessage will
     *                  elaborate
     */
    public DatastoreQueryResponse (int resCode,
                                   boolean resFlag,
                                   String resMessage,
                                   Vector<VersionData> resVector,
                                   Object resObject) {

        this.responseCode = checkNotNull(resCode);
        this.responseFlag = resFlag;
        this.responseMessage = checkNotNull(resMessage);
        this.resultVector = resVector;
        this.responseObject = resObject;
    }

    /**
     * Method to get the response code
     * @return response code
     */
    public int getResponseCode() {
        return responseCode;
    }

    /**
     * Method to get the response flag
     * @return response flag, true/false
     */
    public boolean isResponseFlag() {
        return responseFlag;
    }

    /**
     * Method to get the response message
     * @return response message
     */
    public String getResponseMessage() {
        return responseMessage;
    }

    /**
     * Method to get the resulting commit vector, if
     * present. If query generates anything other than
     * a vector, responseMessage will contain information
     * and responseObject will hold generic information.
     * @return result commit vector
     */
    public Vector<VersionData> getResultVector() {
        return resultVector;
    }

    /**
     * Method to get the fall-back response object. This is
     * a POJO that holds information if the query did not
     * generate a response vector. responseMessage will
     * elaborate on what information is held.
     * @return response object
     */
    public Object getResponseObject() {
        return responseObject;
    }
}
