/*
 * Copyright 2014 Open Networking Laboratory
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
 
package org.onosproject.netmanager.impl;

import java.util.Map;
import java.util.Vector;

/**
 * A demonstrative service for the version control on SDN systems
 */
public interface NetManagerService {
 
    /**
    * A set of methods to provide revision control services to the
    * controller ecosystem
    */

    /**
     * Chronologically orders and displays a historical list of
     commits from the datastore
     * @return sorted immutable table view of all commits from
     * datastore
     */
    DatastoreQueryResponse netManagerStateLog();

    /**
     * Describes an individual commit in detail
     * @param commitId the commit to be described
     * @return Map of flow-id:commit versions grouped under
     * this commit-id
     */
    DatastoreQueryResponse netManagerCommitShow(String commitId);

    /**
     * Displays the historical evolution of a flow or commit, depending on which
     * id is provided. If both commit-id and flow-id are null, it displays the
     * diff between the last two versions of the latest commit chronologically.
     *
     * @param commitHash commit-id to be searched for
     * @param flowId flow-id to be searched for
     * @param v1 first anchor version for diff
     * @param v2 second anchor version for diff
     * @return vector containing diff information for filtered result
     */
    DatastoreQueryResponse netManagerVersionDiff (String commitHash, Long flowId, int v1, int v2);

    /**
     * Generic filtering method to query the object store based on the ids
     * provided
     * @param cliParams hashmap of command line params
     * @return vector of commit objects that matches the query
     */
    DatastoreQueryResponse netManagerFind (Map<String, Object> cliParams);

    /**
     * Command to show which author changed what version
     * of a flow
     * @param flowId flow-id for which blame info was requested
     * @return vector of commit objects for corresponding flow
     */
    DatastoreQueryResponse netManagerVersionBlame (long flowId);

    /**
     * Command to reset a flow to its original state or HEAD
     * (v1 is considered to be HEAD in most cases, unless noted
     * otherwise)
     * @param commitId the commit-id to reset
     * @return true if reset was successful, false otherwise
     */
    DatastoreQueryResponse netManagerVersionReset (String commitId);

}