/*
 * Copyright 2014 Open Networking Laboratory
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.onosproject.netmanager.cli;

import com.google.common.collect.ImmutableList;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.karaf.shell.commands.Argument;
import org.apache.karaf.shell.commands.Command;
import org.onosproject.cli.AbstractShellCommand;
import org.onosproject.netmanager.impl.ColorCodes;
import org.onosproject.netmanager.impl.DatastoreQueryResponse;
import org.onosproject.netmanager.impl.NetManagerService;
import org.onosproject.netmanager.impl.VersionData;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.Vector;

/**
 * Command to describe a commit in detail
 */
@Command(scope = "onos", name = "nm-diff",
        description = "Lists the endpoints for which intents are installed")
public class NetManagerVersionDiffCommand extends AbstractShellCommand {

    private final ImmutableList<String> displaySkipList =
            new ImmutableList.Builder<String>()
                    .add("commitTimestamp", "log", "dateTime", "commitId")
                    .build();

    @Argument(index = 0, name = "commit-id", description = "ID of commit to be searched",
            required = true, multiValued = false)
    String commitId = null;
    @Argument(index = 1, name = "flow-id", description = "ID of flow to index",
            required = true, multiValued = false)
    Long flowId = null;
    @Argument(index = 2, name = "version1", description = "first version anchor",
            required = false, multiValued = false)
    int v1 = 0;
    @Argument(index = 3, name = "version2", description = "second version anchor",
            required = false, multiValued = false)
    int v2 = 0;

    // data structure to hold the response
    DatastoreQueryResponse cmdResponse;
    String dateFormat = "EEE, MMM dd HH:mm:ss yyyy ";

    @Override
    protected void execute() {

        // get a reference to the VC Service
        NetManagerService vcService = AbstractShellCommand.get(NetManagerService.class);
        cmdResponse = vcService.netManagerVersionDiff(commitId, flowId, v1, v2);
        /*
        if (v1 == 0 || v2 == 0) {
            System.out.println(ColorCodes.ANSI_YELLOW +
                    "Anchor version absent. Diff-ing two most recent versions");
        }
        else {
            computeDelta(commitVector.elementAt(commitVector.size()-1),
                    commitVector.elementAt(commitVector.size()-2));
        }
        */
    }

    /**
     * Internal method to compute the delta between two version objects
     * @param existingCommit commit from datastore
     * @param newCommit newly prepared commit version
     */
    private void computeDelta(VersionData existingCommit,
                                       VersionData newCommit) {

        // If the entity is null or has no ID, it hasn't been persisted before,
        // so there's no delta to calculate
        if (newCommit == null) {
            return;
        }
        Field[] fields = ArrayUtils.addAll(newCommit.getClass().getDeclaredFields(),
                VersionData.class.getDeclaredFields());
        Set<Field> uniqueFields = new HashSet<>(Arrays.asList(fields));
        Object oldField = null;
        Object newField = null;
        StringBuilder delta = new StringBuilder();
        for (Field field : uniqueFields) {
            field.setAccessible(true); // We need to access private fields
            try {
                oldField = field.get(existingCommit);
                newField = field.get(newCommit);
            } catch (IllegalArgumentException e) {
                log.error("Bad argument given");
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                log.error("Could not access the argument");
                e.printStackTrace();
            }
            if ((oldField != newField)
                    && (((oldField != null) &&
                    !oldField.equals(newField)) ||
                    ((newField != null) &&
                            !newField.equals(oldField)))) {
                delta.append(field.getName())
                        .append(": [")
                        .append(oldField)
                        .append("] -> [")
                        .append(newField)
                        .append("]  ");
                if(displaySkipList.contains(field.getName())) {
                    continue;
                }
                else {
                    System.out.println("Field:\t " + field.getName());
                    System.out.println(ColorCodes.ANSI_RED + oldField
                            + ColorCodes.ANSI_RESET);
                    System.out.println(ColorCodes.ANSI_GREEN + newField
                            + ColorCodes.ANSI_RESET);
                    System.out.println("\n");
                }
            }
        }
    }
}