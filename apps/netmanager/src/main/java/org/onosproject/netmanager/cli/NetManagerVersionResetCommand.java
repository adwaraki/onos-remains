/*
 * Copyright 2014 Open Networking Laboratory
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.onosproject.netmanager.cli;

import org.apache.karaf.shell.commands.Argument;
import org.apache.karaf.shell.commands.Command;
import org.onosproject.cli.AbstractShellCommand;
import org.onosproject.core.UnavailableIdException;
import org.onosproject.netmanager.impl.ColorCodes;
import org.onosproject.netmanager.impl.DatastoreQueryResponse;
import org.onosproject.netmanager.impl.NetManagerService;

/**
 * Command to describe a commit in detail
 */
@Command(scope = "onos", name = "nm-reset",
        description = "Resets a specified flow to its HEAD version")
public class NetManagerVersionResetCommand extends AbstractShellCommand {

    @Argument(index = 0, name = "commitId", description = "ID of commit to be reset",
            required = true, multiValued = false)
    String id = null;

    DatastoreQueryResponse cmdResponse;
    boolean resetStatus;

    @Override
    protected void execute() {

        // get a reference to the VC Service
        NetManagerService vcService = AbstractShellCommand.get(NetManagerService.class);
        if(id == null) {
            log.error("Need commit id to reset version");
            throw new UnavailableIdException("Commit id not present");
        }
        else {
            cmdResponse = vcService.netManagerVersionReset(id);
            int resCode = cmdResponse.getResponseCode();
            resetStatus = cmdResponse.isResponseFlag();
            if (resCode == 99) {
                System.out.println(cmdResponse.getResponseMessage());
                System.out.println(ColorCodes.ANSI_RED + "Error fetching commit log, " +
                        "check system log for details");
            } else if (resetStatus && (resCode == 0)) {
                System.out.println("Version reset successful");
                log.info("Version reset successful");
            } else {
                log.error("This condition should not occur currently");
            }
        }
    }
}