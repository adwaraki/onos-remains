/*
 * Copyright 2014 Open Networking Laboratory
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
 
package org.onosproject.netmanager.impl;

import com.google.common.base.MoreObjects;
import com.googlecode.cqengine.attribute.Attribute;
import com.googlecode.cqengine.attribute.MultiValueAttribute;
import com.googlecode.cqengine.attribute.SimpleAttribute;
import com.googlecode.cqengine.attribute.SimpleNullableAttribute;
import com.googlecode.cqengine.query.option.QueryOptions;
import org.apache.commons.lang3.ArrayUtils;
import org.joda.time.DateTime;
import org.joda.time.base.AbstractDateTime;
import org.onosproject.net.flow.TrafficSelector;
import org.onosproject.net.flow.TrafficTreatment;
import org.onosproject.net.flow.criteria.Criterion;
import org.onosproject.net.flow.criteria.EthCriterion;
import org.onosproject.net.flow.criteria.IPCriterion;
import org.onosproject.store.service.WallClockTimestamp;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.EnumSet;
import java.util.Objects;

import static com.google.common.base.Preconditions.checkNotNull;
 
/**
 * A demonstrative service for the version control on SDN systems
 */
public class VersionData implements Comparable<VersionData> {
    /*
    * Data model for the versioning mechanism
    */
    private int version;
    private boolean isRuleDefault;
    private boolean isActiveVersion;
    private EnumSet<VersionDataState> versionState;
	private final short authorId;
    private final String authorName;
    private final int priority;
    private final String deviceId;
    private final int tableId;
    private final long flowId;
	private final WallClockTimestamp commitTimestamp;
	private final CommitId commitId;
    private final TrafficSelector ruleSelector;
    private final TrafficTreatment ruleTreatment;

    private final AbstractDateTime dateTime;

    private final Logger log = LoggerFactory.getLogger(getClass());

    /**
     * CQEngine attribute definitions
     */
    public static final Attribute<VersionData, Integer> VERSION_ID =
            new SimpleAttribute<VersionData, Integer>("versionId") {
                @Override
                public Integer getValue(VersionData version,
                                        QueryOptions queryOptions)
                { return version.getVersion(); }
            };

    public static final Attribute<VersionData, Short> APP_ID =
            new SimpleAttribute<VersionData, Short>("appId") {
                @Override
                public Short getValue(VersionData version, QueryOptions queryOptions)
                { return version.getAuthorId(); }
            };

    public static final Attribute<VersionData, String> COMMIT_ID =
            new SimpleAttribute<VersionData, String>("commitId") {
                @Override
                public String getValue(VersionData version, QueryOptions queryOptions)
                { return version.getCommitId().stringValue(); }
            };

    public static final Attribute<VersionData, Long> FLOW_ID =
            new SimpleAttribute<VersionData, Long>("commitId") {
                @Override
                public Long getValue(VersionData version, QueryOptions queryOptions)
                { return version.getFlowId(); }
            };

    public static final Attribute<VersionData, Integer> TABLE_ID =
            new SimpleAttribute<VersionData, Integer>("tableId") {
                @Override
                public Integer getValue(VersionData version, QueryOptions queryOptions)
                { return version.getTableId(); }
            };

    public static final Attribute<VersionData, String> DEVICE_ID =
            new SimpleAttribute<VersionData, String>("deviceId") {
                @Override
                public String getValue(VersionData version, QueryOptions queryOptions)
                { return version.getDeviceId(); }
            };

    public static final Attribute<VersionData, String> SRC_MAC =
            new SimpleNullableAttribute<VersionData, String>("srcMac") {
                @Override
                public String getValue(VersionData version, QueryOptions queryOptions) {
                    try {
                        EthCriterion srcMacFilter =
                                (EthCriterion) version.ruleSelector
                                        .getCriterion(Criterion.Type.ETH_SRC);
                            return createBinaryString(srcMacFilter.mac()
                                    .toStringNoColon().getBytes("utf-8"));
                    } catch (UnsupportedEncodingException e) {
                        return null;
                    } catch (NullPointerException e) {
                        return null;
                    }
                }
            };

    public static final Attribute<VersionData, String> SRC_IP =
            new SimpleNullableAttribute<VersionData, String>("srcIP") {
                @Override
                public String getValue(VersionData version, QueryOptions queryOptions)
                {
                    try {
                        IPCriterion srcIPFilter =
                                (IPCriterion)version.ruleSelector
                                        .getCriterion(Criterion.Type.IPV4_SRC);
                        return(createBinaryString(srcIPFilter.ip().address().toOctets()));
                    } catch (NullPointerException e) {
                        return null;
                    }
                }
            };

    public static final Attribute<VersionData, Long> TIMESTAMP =
            new SimpleAttribute<VersionData, Long>("timeStamp") {
                @Override
                public Long getValue(VersionData version, QueryOptions queryOptions)
                { return version.getCommitTimestamp().unixTimestamp(); }
            };

    public static final Attribute<VersionData, VersionDataState>
            STATE_ATTRIBUTES =
            new MultiValueAttribute<VersionData, VersionDataState>("stateVars") {
                @Override
                public Iterable<VersionDataState>
                    getValues(VersionData version,
                              QueryOptions queryOptions) {
                    return version.versionState;
                }
            };


    /**
     * Constructor for the VersionData class
     * @param appId ID of application authoring change
     * @param deviceId ID of device on which rule resides
     * @param tableId table ID of associated rule
     * @param tStamp timestamp of commit version creation
     * @param cID commit ID generated from contents
     * @param selector rule traffic selector information
     * @param treatment rule traffic treatment information
     */
	public VersionData (short appId, String appName,
                        int priority, String deviceId,
                        int tableId, long flowId,
                        WallClockTimestamp tStamp,
                        CommitId cID, TrafficSelector selector,
                        TrafficTreatment treatment) {

		this.authorId = checkNotNull(appId);
        this.authorName = checkNotNull(appName);
        this.priority = checkNotNull(priority);
        this.deviceId = deviceId;
        this.tableId = checkNotNull(tableId);
        this.flowId = checkNotNull(flowId);
		this.commitTimestamp = checkNotNull(tStamp);
		this.commitId = checkNotNull(cID);
        this.ruleSelector = selector;
        this.ruleTreatment = treatment;
        this.dateTime = new DateTime(commitTimestamp.unixTimestamp());
        this.versionState = EnumSet.noneOf(VersionDataState.class);
	}

    /**
     * Method to get the commit version
     * @return version-id of the current commit object
     */
    public int getVersion() {
        return version;
    }

    /**
     * Method to set the version of the commit object,
     * normally version vector size +1
     * @param version value to set for version-id
     */
    public void setVersion(int version) {
        this.version = checkNotNull(version);
    }

    /**
     * Method to check if a version's state variable is set
     * @return true if the
     */
    public boolean isStateVariableSet(VersionDataState stateVar) {
        return versionState.contains(stateVar);
    }

    /**
     * Method to set the state variables on the version
     * @param states list of version state variables
     */
    public void setStateVars(VersionDataState... states) {
        this.versionState = EnumSet.copyOf(Arrays.asList(states));
    }

    public void activateStateVar(VersionDataState p) {
        versionState.add(p);
    }

    public void deactivateStateVar(VersionDataState p) {
        versionState.remove(p);
    }

    /**
     * Method to check if this rule is part of the default set
     * or not
     * @return true if default, else false
     */
    public boolean isRuleDefault() {
        return this.isRuleDefault;
    }

    /**
     * Method to set rule's default status
     * @param isRuleDefault
     */
    public void setIsRuleDefault(boolean isRuleDefault) {
        this.isRuleDefault = isRuleDefault;
    }

    /**
     * Method to check if this is the currently programmed version on
     * the switch
     * @return true if this is latest version on switch, false otherwise
     */
    public boolean isActiveVersion() {
        return this.isActiveVersion;
    }

    public void setIsActiveVersion(boolean isActiveVersion) {
        this.isActiveVersion = isActiveVersion;
    }
    /**
     * Method to return the stored commitId
     * @return commit Id associated with this VersionData object
     */
    public CommitId getCommitId() {
        return this.commitId;
    }

    /**
     * Method to return the stored author id
     * @return author Id associated with this VersionData object
     */
    public short getAuthorId() {
        return this.authorId;
    }

    /**
     * Method to return the app's name associated with this commit
     * @return string containing app name
     */
    public String getAuthorName() {
        return authorName;
    }

    /**
     * Method to return the stored device id
     * @return device Id associated with this VersionData object
     */
    public String getDeviceId() {
        return this.deviceId;
    }

    /**
     * Method to get the rule's priority associated with this
     * commit
     * @return rule priority
     */
    public int getPriority() {
        return priority;
    }

    /**
     * Method to return the table ID associated with this commit's
     * rule
     * @return commit Id associated with this VersionData object
     */
    public int getTableId() {
        return this.tableId;
    }

    /**
     * Method to return the flow ID associated with this commit
     * @return flowId object
     */
    public long getFlowId() {
        return flowId;
    }

    /**
     * Method to return the stored commitId
     * @return commit Id associated with this VersionData object
     */
    public WallClockTimestamp getCommitTimestamp() {
        return this.commitTimestamp;
    }

    /**
     * Method to get the selector associated with the stored rule
     * @return trafficselector object for this commit's rule
     */
    public TrafficSelector getRuleSelector() { return ruleSelector; }

    /**
     * Method to return the commit's rule treatment info
     * @return TrafficTreatment object for commit
     */
    public TrafficTreatment getRuleTreatment() {
        return ruleTreatment;
    }

    /**
     * Method to get the commit's JODA timestamp representation
     * @return JODA datetime object
     */
    public DateTime getDateTime() {
        return (DateTime)dateTime;
    }

    @Override
    public boolean equals(Object other) {
        if (this == other) {
            return true;
        }
        //
        // NOTE: Subclasses are considered as change of identity, hence
        // equals() will return false if the class type doesn't match.
        //
        if (other == null || getClass() != other.getClass()) {
            return false;
        }
        VersionData that = (VersionData) other;
        if((this.authorId == that.authorId) &&
            (this.deviceId).equals(that.deviceId) &&
            ((this.flowId == that.flowId)) &&
            (this.tableId == that.tableId) &&
            //TODO - removing commit-id for now since initial state is committed twice
            // (this.commitId.stringValue().equals(that.commitId.stringValue())) &&
            //((this.ruleSelector).equals(that.ruleSelector)) &&
            Objects.equals(this.ruleSelector, that.ruleSelector) &&
            //((this.ruleTreatment).equals(that.ruleTreatment)) &&
            Objects.equals(this.ruleTreatment, that.ruleTreatment) &&
            (this.priority == that.priority)) {
            log.debug("Version data objects are the same");
            return true;
        }
        else {
            log.debug("Version data objects are not the same");
            computeDelta(this, that);
            return false;
        }
    }

    @Override
    public int hashCode() {
        return Objects.hash(authorId, deviceId, tableId,
                priority, commitTimestamp, commitId,
                ruleSelector);
    }

    @Override
    public String toString() {
        //TODO - Need a more meaningful implementation of toString
        return MoreObjects.toStringHelper(getClass())
            .omitNullValues()
            .add("version", version)
            .add("author-id", authorId)
            .add("flow-id", flowId)
            .add("commit-id", commitId.stringValue())
            .add("priority", priority)
            .add("device-id", deviceId)
            .add("table-id", tableId)
            .add("rule-criteria", ruleSelector)
            .add("rule-treatment", ruleTreatment)
            .toString();
    }

    @Override
    public int compareTo(VersionData that) {
        return (this.dateTime.compareTo(that.dateTime));
    }

    /**
     * Internal method to compute the delta between two version objects
     * @param existingCommit commit from datastore
     * @param newCommit newly prepared commit version
     */
    private StringBuilder computeDelta(VersionData existingCommit,
                                       VersionData newCommit) {

        // If the entity is null or has no ID, it hasn't been persisted before,
        // so there's no delta to calculate
        if (newCommit == null) {
            return null;
        }
        Field[] fields = ArrayUtils.addAll(newCommit.getClass().getDeclaredFields(),
                VersionData.class.getDeclaredFields());
        Object oldField = null;
        Object newField = null;
        StringBuilder delta = new StringBuilder();
        for (Field field : fields) {
            field.setAccessible(true); // We need to access private fields
            try {
                oldField = field.get(existingCommit);
                newField = field.get(newCommit);
            } catch (IllegalArgumentException e) {
                log.error("Bad argument given");
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                log.error("Could not access the argument");
                e.printStackTrace();
            }
            if ((oldField != newField)
                    && (((oldField != null) &&
                    !oldField.equals(newField)) ||
                    ((newField != null) &&
                            !newField.equals(oldField)))) {
                delta.append(field.getName()).append(": [").append(oldField).append("] -> [")
                        .append(newField).append("]  ");
            }
        }
        log.info("Version Delta Info : " + delta);
        return delta;
    }

    /**
     * Creates the binary string representation of an IP prefix.
     * The prefix can be either IPv4 or IPv6.
     * The string length is equal to the prefix length.
     *
     * @param inputData the input data to convert
     * @return the binary string representation
     */
    public static String createBinaryString(byte[] inputData) {
        if (inputData.length == 0) {
            return "";
        }

        StringBuilder result = new StringBuilder(inputData.length);
        for (int i = 0; i < inputData.length; i++) {
            int byteOffset = i / Byte.SIZE;
            int bitOffset = i % Byte.SIZE;
            int mask = 1 << (Byte.SIZE - 1 - bitOffset);
            byte value = inputData[byteOffset];
            boolean isSet = ((value & mask) != 0);
            result.append(isSet ? "1" : "0");
        }
        return result.toString();
    }

    public enum VersionDataState {
        DEFAULT, ACTIVE, PERMANENT, RESET;
    }
}

	