/*
 * Copyright 2014 Open Networking Laboratory
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.onosproject.netmanager.impl;

import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Deactivate;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.ReferenceCardinality;
import org.apache.felix.scr.annotations.Service;

import org.onosproject.core.ApplicationId;
import org.onosproject.app.ApplicationIdStore;
import org.onosproject.core.CoreService;
import org.onosproject.net.Device;
import org.onosproject.net.DeviceId;
import org.onosproject.net.device.DeviceService;
import org.onosproject.net.flow.*;
import org.onosproject.net.flowobjective.DefaultForwardingObjective;
import org.onosproject.net.flowobjective.FlowObjectiveService;
import org.onosproject.net.flowobjective.ForwardingObjective;
import org.onosproject.store.service.WallClockTimestamp;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Field;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.bitbucket.cowwoc.diffmatchpatch.DiffMatchPatch;
import org.bitbucket.cowwoc.diffmatchpatch.DiffMatchPatch.Diff;
import org.bitbucket.cowwoc.diffmatchpatch.DiffMatchPatch.Patch;
import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Ordering;
import com.google.common.collect.Table;
import org.apache.commons.lang3.ArrayUtils;

import com.googlecode.cqengine.ConcurrentIndexedCollection;
import com.googlecode.cqengine.IndexedCollection;
import com.googlecode.cqengine.index.hash.HashIndex;
import com.googlecode.cqengine.index.navigable.NavigableIndex;
import com.googlecode.cqengine.index.radix.RadixTreeIndex;
import com.googlecode.cqengine.resultset.ResultSet;
import com.googlecode.cqengine.query.Query;
import com.googlecode.cqengine.query.logical.And;

import java.util.*;

import static com.googlecode.cqengine.query.QueryFactory.*;
import static com.googlecode.cqengine.query.option.EngineThresholds.INDEX_ORDERING_SELECTIVITY;

@Component(immediate = true)
@Service
public class NetManager implements NetManagerService {


    /****************************************************************************************
     ************************     INITIAL STATE SETUP SECTION    ****************************
     ****************************************************************************************/

    private static final int DEFAULT_TIMEOUT = 10;
    private static final int DEFAULT_PRIORITY = 10;

    private final Logger log = LoggerFactory.getLogger(getClass());
    private final FlowRuleListener listener = new VCFlowRuleListener();
    private ApplicationId appId;

    //TODO - To be removed
    /**
     * Version Control Data Structures Section
     * Version 1.1
     * Simultaneously testing the use of Guava Tables as a datastore
     */
    private Table<String, Long, Vector<VersionData>>
            tableBackedVersionStore = HashBasedTable.create();

    /**
     * CQEngine backed data structures for storing information
     */
    IndexedCollection<VersionData> cqEngineDataStore =
            new ConcurrentIndexedCollection<>();

    @Reference(cardinality = ReferenceCardinality.MANDATORY_UNARY)
    protected FlowRuleService flowRuleService;

    @Reference(cardinality = ReferenceCardinality.MANDATORY_UNARY)
    protected ApplicationIdStore idStore;

    @Reference(cardinality = ReferenceCardinality.MANDATORY_UNARY)
    protected CoreService coreService;

	@Reference(cardinality = ReferenceCardinality.MANDATORY_UNARY)
    protected DeviceService deviceService;

    @Reference(cardinality = ReferenceCardinality.MANDATORY_UNARY)
    protected FlowObjectiveService flowObjectiveService;

    @Activate
    public void activate() {

        appId = coreService.registerApplication("org.onosproject.netmanager");
        flowRuleService.addListener(listener);
        log.info("Preparing initial commit version of network state");
        commitCurrentState();
        log.info("Started with Application ID {}", appId.id());
        // testDiffingPatching();
        cqEngineDataStore.addIndex(NavigableIndex.onAttribute(VersionData.VERSION_ID));
        cqEngineDataStore.addIndex(NavigableIndex.onAttribute(VersionData.APP_ID));
        cqEngineDataStore.addIndex(HashIndex.onAttribute(VersionData.COMMIT_ID));
        cqEngineDataStore.addIndex(NavigableIndex.onAttribute(VersionData.FLOW_ID));
        cqEngineDataStore.addIndex(NavigableIndex.onAttribute(VersionData.DEVICE_ID));
        cqEngineDataStore.addIndex(NavigableIndex.onAttribute(VersionData.TABLE_ID));
        cqEngineDataStore.addIndex(RadixTreeIndex.onAttribute(VersionData.SRC_MAC));
        cqEngineDataStore.addIndex(RadixTreeIndex.onAttribute(VersionData.SRC_IP));
        cqEngineDataStore.addIndex(NavigableIndex.onAttribute(VersionData.TIMESTAMP));
        cqEngineDataStore.addIndex(HashIndex.onAttribute(VersionData.STATE_ATTRIBUTES));
    }

    @Deactivate
    public void deactivate() {
        // TODO revoke all packet requests when deactivate
        flowRuleService.removeListener(listener);
        log.info("Stopped");
    }

    /****************************************************************************************
     **************************     CLI COMMANDS SUPPORT SECTION    *************************
     ****************************************************************************************/

    /**
     * Overridden method to return the commit vector from the cqengine-backed datastore
     * @return commit map of commitid->versiondata objects
     */
    @Override
    public DatastoreQueryResponse netManagerStateLog() {

        Vector<VersionData> resVector = sortDataStoreCommits(null);
        if (resVector == null) {
            String resMessage = "Commit sorter returned null";
            return(new DatastoreQueryResponse(99, false,
                            resMessage, null, null));
        }
        else {
            String resMessage = "Sorted commits in vector";
            return(new DatastoreQueryResponse(0, true,
                    resMessage, resVector, null));
        }
    }
    /**
     * Overridden method to return a vector of <versiondata> for
     * a given commit id
     * @param commitId id of commit whose information has been requested
     * @return Vector<VersionData> of commit information
     */
    @Override
    public DatastoreQueryResponse netManagerCommitShow(String commitId) {

        log.info("Retrieving information for commit id : " + commitId);
        Vector<VersionData> matchedCommits = new Vector<>();
        cqEngineDataStore.retrieve(equal(VersionData.COMMIT_ID,
                commitId)).forEach(matchedCommits::add);
        if (matchedCommits == null) {
            String resMessage = "No commits retrieved from datastore";
            return(new DatastoreQueryResponse(99, false,
                    resMessage, null, null));
        }
        else {
            String resMessage = "Corresponding commits in vector";
            return(new DatastoreQueryResponse(0, true,
                    resMessage, matchedCommits, null));

        }
    }

    /**
     * Displays the historical evolution of a flow or commit, depending on which
     * id is provided. If both commit-id and flow-id are null, it displays the
     * diff between the last two versions of the latest commit chronologically.
     *
     * @param commitHash commit-id to be searched for
     * @param flowId flow-id to be searched for
     * @param v1 first anchor version for diff
     * @param v2 second anchor version for diff
     * @return vector containing diff information for filtered result
     */
    @Override
    public DatastoreQueryResponse
    netManagerVersionDiff(String commitHash, Long flowId, int v1, int v2) {
        /*
        // Have to ensure that v2 is always greater than v1
        if(v1 == 0 || v2 == 0) {
            // If either version anchors are 0, then diff the last and penultimate
            // commit versions for that flow.
            log.debug("Retrieving information for commit id : " + commitHash);
            Vector<VersionData> matchedCommits = new Vector<>();
            cqEngineDataStore.retrieve(and(equal(VersionData.COMMIT_ID, commitHash),
                    equal(VersionData.FLOW_ID, flowId))).forEach(matchedCommits::add);

            if (matchedCommits.size() == 1) {
                log.info("Single entry in vector for commit : " + entry.getValue());
                return matchedCommits.lastElement().getValue();
            } else {
                int size = matchedCommits.size();
                computeDelta(matchedCommits.elementAt(size - 1),
                        matchedCommits.elementAt(size - 2));
                return entry.getValue();
            }
        }
        */
        return(new DatastoreQueryResponse(99, false,
                "", null, null));

    }

    /**
     * Method to retrieve relevant change info for a give flow
     * @param flowId flow-id for which blame info was requested
     * @return vector of commit versions for given flow-id
     */
    @Override
    public DatastoreQueryResponse netManagerVersionBlame(long flowId) {
        Vector<VersionData> filteredCommits = new Vector<>();
        cqEngineDataStore.retrieve(equal(VersionData.FLOW_ID,
                flowId)).forEach(filteredCommits::add);
        if (filteredCommits == null) {
            String resMessage = "No commits retrieved from datastore";
            return(new DatastoreQueryResponse(99, false,
                    resMessage, null, null));
        }
        Vector<VersionData> sortedCommits =
                sortDataStoreCommits(filteredCommits);
        if (sortedCommits == null) {
            String resMessage = "Commit sorter returned null";
            return(new DatastoreQueryResponse(99, false,
                    resMessage, null, null));
        } else {
            String resMessage = "Commits matching flow-id populated in vector";
            return(new DatastoreQueryResponse(0, true,
                    resMessage, sortedCommits, null));
        }
    }

    /**
     * Method to reset a certain commit to its original version,
     * or HEAD. In most cases, HEAD=v1, unless noted otherwise.
     * 1. Retrieve all versions for commit-id, in the case of grouped
     * commits, order commits by flow-id next.
     * 2.
     *   (a) If flow-id is not specified, reset all flows AFTER
     *   confirmation.
     *   (b) If flow-id is specified, reset specified flow AFTER
     *   confirmation.
     * 3. A RESET involves pulling information from the first version,
     * rebuilding the rule and pushing it to the data-store.
     * 4. The current design should automatically handle this as a new
     * version and commit it to the data-store.
     *
     * @param commitId the commit-id to reset
     * @return true if reset was successful, false otherwise
     */
    @Override
    public DatastoreQueryResponse netManagerVersionReset(String commitId) {
        ForwardingObjective forwardingObjective;
        VersionData resetVersion =
                cqEngineDataStore.retrieve(and(equal(VersionData.COMMIT_ID, commitId),
                        equal(VersionData.VERSION_ID, 1))).iterator().next();
        if(resetVersion == null) {
            log.error("HEAD(v1) for commit not found, cannot reset");
            String resMessage = "HEAD(v1) for commit not found, cannot reset";
            return(new DatastoreQueryResponse(99, false,
                    resMessage, null, null));
        } else {
            try {

                if(resetVersion.isStateVariableSet
                        (VersionData.VersionDataState.PERMANENT)) {
                    forwardingObjective = DefaultForwardingObjective.builder()
                            .withSelector(resetVersion.getRuleSelector())
                            .withTreatment(resetVersion.getRuleTreatment())
                            .withPriority(resetVersion.getPriority())
                            .fromApp(idStore.getAppId(resetVersion.getAuthorId()))
                            .makePermanent()
                            .withFlag(ForwardingObjective.Flag.VERSATILE)
                            .add();

                } else {
                    forwardingObjective = DefaultForwardingObjective.builder()
                            .withSelector(resetVersion.getRuleSelector())
                            .withTreatment(resetVersion.getRuleTreatment())
                            .withPriority(resetVersion.getPriority())
                            .fromApp(idStore.getAppId(resetVersion.getAuthorId()))
                            .makeTemporary(DEFAULT_TIMEOUT)
                            .withFlag(ForwardingObjective.Flag.VERSATILE)
                            .add();
                }
                flowObjectiveService.forward
                        (DeviceId.deviceId(resetVersion.getDeviceId()),
                                forwardingObjective);
            } catch (Exception e) {
                e.printStackTrace();
                log.error("Error resetting version");
                String resMessage = "Error resetting version, check log for details";
                return(new DatastoreQueryResponse(99, false,
                        resMessage, null, null));
            }
        }
        String resMessage = "Flow reset successfully to HEAD";
        return(new DatastoreQueryResponse(0, true,
                resMessage, null, null));
    }

    /**
     * Method to find commits according to given parameters in the query. All commits
     * that match the specificied parameter list from the command-line as used as a filter
     * set to be sent to CQEngine.
     * @param incomingParams filter parameters from the command-line
     * @return <VersionData> vector containing list of commits
     */
    @Override
    public DatastoreQueryResponse netManagerFind(Map<String, Object> incomingParams) {
        /*
        If none of the parameters are used, then it is is equivalent
        to doing a netManager-log
         */
        Vector<VersionData> filteredVector = new Vector<>();
        Vector<VersionData> resVector;
        Collection<Query<VersionData>> filters = new ArrayList<>();
        for(Map.Entry<String, Object> queryFilter :
                incomingParams.entrySet()) {
            String key = queryFilter.getKey();
            Object value = queryFilter.getValue();
            switch(key) {
                //build the query based on the filter set
                case "app-id":
                    if((short) value <= 0) {
                        log.info("app-id absent in filter set");
                    } else {
                        filters.add(equal(VersionData.APP_ID, (short) value));
                        log.info("Added app-id to filter list");
                    }
                    break;

                case "commit-id":
                    if(value == null) {
                        log.info("commit-id absent in filter set");
                    }
                    else {
                        filters.add(equal(VersionData.COMMIT_ID, (String) value));
                        log.info("Added commit-id to filter list");
                    }
                    break;

                case "flow-id":
                    if((long) value == 0) {
                        log.info("flow-id absent in filter set");
                    } else {
                        filters.add(equal(VersionData.FLOW_ID, (long) value));
                        log.info("Added flow-id to filter list");
                    }
                    break;

                case "table-id":
                    if((int) value < 0) {
                        log.info("table-id absent in filter set");
                    } else {
                        filters.add(equal(VersionData.TABLE_ID, (int) value));
                        log.info("Added table-id to filter list");
                    }
                    break;

                case "device-id":
                    if(value == null) {
                        log.info("device-id absent in filter set");
                    } else {
                        filters.add(equal(VersionData.DEVICE_ID,
                                (String) value));
                        log.info("Added device-id to filter list");
                    }
                    break;

                case "src-mac":
                    if(value == null) {
                        log.info("src-mac absent in filter set");
                    } else {
                        String srcMac = (String) value;
                        try {
                            filters.add(startsWith(VersionData.SRC_MAC,
                                    VersionData.createBinaryString
                                            (srcMac.getBytes("utf-8"))));
                        } catch (UnsupportedEncodingException e) {
                            log.error("Unsupported enconding format");
                            return null;
                        }
                        log.info("Added src-mac to filter list");
                    }
                    break;

                case "active":
                    if(value == null) {
                        log.info("active status absent in filter set");
                    } else {
                        filters.add(equal(VersionData.STATE_ATTRIBUTES,
                                VersionData.VersionDataState.ACTIVE));
                        log.info("Added version active status to " +
                                "filter set");
                    }
            }
        }
        log.info("Created query : " + filters.toString());
        // this block builds the query based on the filters that have been provided
        switch(filters.size()) {
            case 0:
                resVector = sortDataStoreCommits(null);
                if (resVector == null) {
                    String resMessage = "No filters, commit sorting unsuccessful" +
                            " for all commits in datastore";
                    return(new DatastoreQueryResponse(99, false,
                            resMessage, null, null));
                }
                else {
                    String resMessage = "No filters. Returning all sorted commits";
                    return(new DatastoreQueryResponse(0, true,
                            resMessage, resVector, null));
                }
            case 1:
                cqEngineDataStore.retrieve(filters.iterator().next()).forEach(filteredVector::add);
                if (filteredVector == null) {
                    String resMessage = "No matching commits for single filter";
                    return(new DatastoreQueryResponse(99, false,
                            resMessage, null, null));
                }
                else {
                    resVector = sortDataStoreCommits(filteredVector);
                    String resMessage = "Returning all sorted commits for single filter";
                    return(new DatastoreQueryResponse(0, true,
                            resMessage, resVector, null));
                }
            default:
                cqEngineDataStore.retrieve((And<VersionData>)filters).forEach(filteredVector::add);
                if (filteredVector == null) {
                    String resMessage = "No matching commits for specified filters";
                    return(new DatastoreQueryResponse(99, false,
                            resMessage, null, null));
                }
                else {
                    resVector = sortDataStoreCommits(filteredVector);
                    String resMessage = "Returning all sorted commits for specified filters";
                    return(new DatastoreQueryResponse(0, true,
                            resMessage, resVector, null));
                }
        }
    }
    /**
     * Internal method to aggregate current network state
     * @return arraylist containing flow entries from current state
     */
	private void commitCurrentState() {

        List<FlowEntry> initialState = new ArrayList<>();
        final Iterable<Device> devices = deviceService.getDevices();

		for (final Device device : devices) {
			final Iterable<FlowEntry> deviceEntries =
				flowRuleService.getFlowEntries(device.id());
			if (deviceEntries != null) {
				for (final FlowEntry entry : deviceEntries) {
                    log.info("Existing flow entry : " + entry.id().toString());
                    initialState.add(entry);
				}
			}
		}
		//TO-DO - Remove the table-backed datastore
        writeToDataStore("TABLE", "BATCH-ADD", initialState);
        writeToDataStore("CQENGINE", "BATCH-ADD", initialState);
    }

    /****************************************************************************************
     *******************     INTERNAL SUPPORT FUNCTIONALITY SECTION    **********************
     ****************************************************************************************/

    /**
     * Internal method that preps a commit version
     * @param rule flow rule that is driving this version change
     * @return commit object containing all pertinent information for the commit
     */
    private VersionData prepareCommitVersion(FlowRule rule,
                                             byte[] groupCommitId,
                                             String ruleOp) {

        log.info("Commit version triggered by app : " + rule.appId());
        ApplicationId appId = idStore.getAppId(rule.appId());
        final WallClockTimestamp commitStamp = new WallClockTimestamp();
        if (groupCommitId == null) {
            byte[] commitId = null;
            String hashInput = new StringBuilder().append(rule.appId())
                    .append(rule.tableId())
                    .append(rule.deviceId().toString())
                    .append(rule.id().toString())
                    .append(rule.priority())
                    .append(rule.selector().criteria().toString()).toString();
            try {
                commitId = computeHash(hashInput);

            } catch (NoSuchAlgorithmException |
                    UnsupportedEncodingException e) {
                log.error("Error computing commit hash");
            }
            try {
                CommitId id = CommitId.valueOf(commitId);
                VersionData commitVersion;
                switch (ruleOp) {
                    case "ADD":
                        log.info("Creating commit version for SINGLE-ADD");
                        commitVersion =
                                new VersionData(rule.appId(),
                                        appId.name(),
                                        rule.priority(),
                                        rule.deviceId().toString(),
                                        rule.tableId(),
                                        rule.id().value(),
                                        commitStamp, id,
                                        rule.selector(),
                                        rule.treatment());
                        break;
                    case "DEL":
                        log.info("Creating commit version for SINGLE-DEL");
                        commitVersion =
                                new VersionData(rule.appId(),
                                        appId.name(),
                                        -1, rule.deviceId().toString(),
                                        rule.tableId(),
                                        rule.id().value(),
                                        commitStamp, id,
                                        null,
                                        null);
                        break;

                    default:
                        log.error("Unknown rule operation, dropping commit info");
                        return null;
                }
                return(commitVersion);
            } catch (Exception e) {
                log.error("Exception encountered while preparing commit");
                return null;
            }
        } else {
            /*
            TODO - ONOS flows with the same flow-ids get messed up in a group commit
            Problem - They are committed as different versions of the same flow, since
            the differentiator is the priority in the hash, but since the group commit-id
            overrides all individual commit-ids, the priority can no longer be used to
            distinguish flows with the same id. The next key is the flow-id itself, which is
            not unique for ONOS, making it a problem.
             */
            try {
                CommitId id = CommitId.valueOf(groupCommitId);
                id.setIsGroupCommit(true);
                VersionData commitVersion;
                switch (ruleOp) {
                    case "ADD":
                        log.info("Creating commit version for GROUP-ADD");
                        commitVersion =
                                new VersionData(rule.appId(),
                                        appId.name(),
                                        rule.priority(),
                                        rule.deviceId().toString(),
                                        rule.tableId(),
                                        rule.id().value(),
                                        commitStamp, id,
                                        rule.selector(),
                                        rule.treatment());
                        break;
                    case "DEL":
                        log.info("Creating commit version for GROUP-DEL");
                        commitVersion =
                                new VersionData(rule.appId(),
                                        appId.name(),
                                        -1, rule.deviceId().toString(),
                                        rule.tableId(),
                                        rule.id().value(),
                                        commitStamp, id,
                                        null,
                                        null);
                        break;

                    default:
                        log.error("Unknown rule operation, dropping commit info");
                        return null;
                }
                return(commitVersion);
            } catch (Exception e) {
                e.printStackTrace();
                log.error("Exception encountered while preparing commit");
                return null;
            }
        }
    }

    private void testDiffingPatching() {

        DiffMatchPatch differ = new DiffMatchPatch();
        LinkedList<Diff> diffs =
                differ.diffMain("This is a test", "This is not a test");
        LinkedList<Patch> patches = differ.patchMake(diffs);
        for(Diff entry : diffs) {
            System.out.println(entry);
        }
        for(Patch entry : patches) {
            System.out.println(entry);
        }
    }
    /**
     * Internal method to convert the cellset to an immutable set for chronological
     * sorting
     * @return ImmutableTable of entries sorted by VersionData.dateTime
     */
    private Vector<VersionData>
        sortDataStoreCommits(Vector<VersionData> commitSet) {

        Vector<VersionData> sortedCommits = new Vector<>();
        //TODO - find ways to optimize this sorting, too complex
        Ordering<VersionData> comparator =
                new Ordering<VersionData>() {
                    public int compare(VersionData cell1,
                                       VersionData cell2) {
                        return cell1.compareTo(cell2);
                    }
                };

        if(commitSet == null) {
        //TO-DO: This method has been replaced by the in-place grouping by
        // CQEngine. Remove this section in a later commit.
        /*
        Vector<VersionData> indexedSortedCommits = new Vector<>();
            // main vector that contains all commits
            Vector<VersionData> masterCommitVector = new Vector<>();
            long start = System.nanoTime();
            cqEngineDataStore.retrieve(all(VersionData.class)).forEach(masterCommitVector::add);
            // That orders cells in increasing order of value, but we want decreasing order...
            comparator.reverse().sortedCopy(masterCommitVector).forEach(sortedCommits::add);
            System.out.println("Non-indexed retrieval and ordering time : "
                    + (System.nanoTime() - start) / 1000 + " us");
            */

            long start = System.nanoTime();
            cqEngineDataStore.retrieve(has(VersionData.TIMESTAMP),
                    queryOptions(orderBy(descending(VersionData.TIMESTAMP)),
                            applyThresholds(threshold(INDEX_ORDERING_SELECTIVITY, 1.0))
                    )).forEach(sortedCommits::add);
            System.out.println("Indexed retrieval and auto-ordering time : "
                     + (System.nanoTime() - start) / 1000 + " us");
            log.info("Cell Set Size : " + sortedCommits.size());
        }
        else {
            log.info("Retrieved commit list : " + commitSet);
            comparator.sortedCopy(commitSet).forEach(sortedCommits::add);
            log.info("Sorted Commit List : " + sortedCommits);
            log.info("Last element in sorted list : "
                    + sortedCommits.lastElement())  ;
        }
        return sortedCommits;
    }
    /**
     * Internal method to provide a layer of abstraction defining which
     * datastore to write the values into
     * @param dataStoreIdentifier string identifying the datastore -
     *                            "TABLE", "CQENGINE" that is passed
     *                            onto the method that does the writing
     * @param dataStoreOp string identifying the operation being performed,
     *                    BATCH-ADD/DEL, SINGLE-ADD/DEL etc.
     * @param ruleList  object that holds either a single rule or a list,
     *                  depending on dataStoreOp being performed
     * @return true or false depending on datastore write status
     */
    private boolean writeToDataStore(String dataStoreIdentifier,
                                     String dataStoreOp,
                                     Object ruleList) {

        if(dataStoreOp.equalsIgnoreCase("BATCH-ADD")) {

            byte[] groupCommitId = null;
            String hashInput = "";

            // First cast the object as an arraylist
            List<FlowEntry> batchRuleList = (ArrayList<FlowEntry>) ruleList;
            log.debug("Bundled rules : " + batchRuleList);

            // Second, prep the group commit-id
            log.debug("Preparing group commit-id from all flows in batch");
            for(FlowEntry entry : batchRuleList) {
                hashInput = new StringBuilder().append(entry.appId())
                                               .append(entry.id().value())
                                               .append(entry.deviceId())
                                               .append(entry.tableId())
                                               .append(entry.priority())
                                               .append(entry.selector())
                                               .toString();
            }
            try {
                groupCommitId = computeHash(hashInput);
            } catch (NoSuchAlgorithmException e) {
                log.error("Error computing group commit-id");
                e.printStackTrace();
                return false;
            } catch (UnsupportedEncodingException e) {
                log.error("Error computing group commit-id");
                e.printStackTrace();
                return false;
            }

            for(FlowEntry entry : batchRuleList) {
                VersionData newCommitVersion =
                        prepareCommitVersion(entry, groupCommitId, "ADD");
                log.info("Pushing flow-id " + entry.id().value()
                        + " to datastore");
                try {
                    pushRuleToDatastore(dataStoreIdentifier,
                            entry, newCommitVersion);
                } catch (Exception e) {
                    e.printStackTrace();
                    log.error("Error inserting into datastore");
                    return false;
                }
            }
        }
        else if(dataStoreOp.equalsIgnoreCase("SINGLE-ADD")){
            FlowEntry flow = (FlowEntry) ruleList;
            VersionData newCommitVersion =
                    prepareCommitVersion(flow, null, "ADD");
            try {
                pushRuleToDatastore(dataStoreIdentifier,
                        flow, newCommitVersion);
            } catch (Exception e) {
                log.error("Error inserting record into datastore");
                e.printStackTrace();
                return false;
            }
        }
        else if(dataStoreOp.equalsIgnoreCase("SINGLE-DEL")){
            FlowEntry flow = (FlowEntry) ruleList;
            VersionData newCommitVersion =
                    prepareCommitVersion(flow, null, "DEL");
            try {
                pushRuleToDatastore(dataStoreIdentifier,
                        flow, newCommitVersion);
                log.info("creating tombstone entry for deleted flow");
            } catch (Exception e) {
                e.printStackTrace();
                log.error("Error deleting record from datastore");
                return false;
            }
        }
        else {
            log.error("This operation is not yet supported");
            return false;

        }
        return true;
    }

    /**
     * Internal method to push records to the datastore
     * @param dataStoreIdentifier id of the datastore to write to
     *                            - purely evaluation based
     * @param rule the flow rule to be committed
     * @param newCommitVersion rule bundled with all commit metadata
     * @return true or false based on insertion success or failure
     */
    private boolean pushRuleToDatastore(String dataStoreIdentifier,
                                        FlowRule rule,
                                        VersionData newCommitVersion) {

        switch(dataStoreIdentifier) {

            ////TO-DO - Remove the table-backed datastore
            case "TABLE":
                if(tableBackedVersionStore.contains(newCommitVersion.getCommitId().stringValue(),
                        rule.id().value())) {
                    Vector<VersionData> existingCommit =
                            tableBackedVersionStore.get(newCommitVersion.getCommitId().stringValue(),
                                    rule.id().value());
                    if(existingCommit.lastElement().equals(newCommitVersion)) {
                        log.info("tablestore : Version data has not changed, " +
                                "not committing");
                        return false;
                    }
                    else {
                        newCommitVersion.setVersion(existingCommit.size() + 1);
                        newCommitVersion.setIsActiveVersion(true);
                        newCommitVersion.activateStateVar
                                (VersionData.VersionDataState.ACTIVE);
                        if(rule.isPermanent()) {
                            newCommitVersion.activateStateVar
                                    (VersionData.VersionDataState.PERMANENT);
                        } else {
                            log.info("Temporary rule, not setting flag");
                        }
                        existingCommit.lastElement().setIsActiveVersion(false);
                        existingCommit.lastElement().
                                deactivateStateVar(VersionData.VersionDataState.ACTIVE);
                        existingCommit.add(newCommitVersion);
                        log.info("tablestore : Rule commit inserted into " +
                                "table backed store");
                    }
                }
                else {
                    Vector<VersionData> newCommitVector = new Vector<>();
                    /* just to ensure that the commit vector is really empty, this value
                       HAS to be 1
                    */
                    newCommitVersion.setVersion(newCommitVector.size() + 1);
                    newCommitVersion.setIsActiveVersion(true);
                    if(rule.isPermanent()) {
                        newCommitVersion.activateStateVar
                                (VersionData.VersionDataState.PERMANENT);
                    } else {
                        log.info("Temporary rule, not setting flag");
                    }
                    newCommitVector.add(newCommitVersion);
                    tableBackedVersionStore.put(newCommitVersion.getCommitId().stringValue(),
                            rule.id().value(), newCommitVector);
                    log.info("tablestore : commit vector does not exist, " +
                            "created vector, inserted commit");
                }
                break;

            case "CQENGINE":
                /*
                cqengine.contains will not work in this case because the timestamp changes,
                and that invalidates object equality in cqengine's case.
                We need to retrieve all objects with the same commit-id, flow-id,
                chronologically arrange them and then add the new object into the datastore
                 */
                log.info("Operative commit version : " + newCommitVersion.toString());
                Query checkFlowQuery = and(equal(VersionData.FLOW_ID,
                                newCommitVersion.getFlowId()),
                        equal(VersionData.COMMIT_ID,
                                newCommitVersion.getCommitId().stringValue()));
                long start;
                Vector<VersionData> allMatchingCommits = new Vector<>();
                Vector<VersionData> orderedCommits;
                ResultSet<VersionData> allCommits =
                        cqEngineDataStore.retrieve(checkFlowQuery);
                if(allCommits.isEmpty()) {
                    newCommitVersion.setVersion(1);
                    newCommitVersion.setIsActiveVersion(true);
                    newCommitVersion.activateStateVar
                            (VersionData.VersionDataState.ACTIVE);
                    if(rule.isPermanent()) {
                        newCommitVersion.activateStateVar
                                (VersionData.VersionDataState.PERMANENT);
                    } else {
                        log.info("Temporary rule, not setting flag");
                    }
                    start = System.nanoTime();
                    cqEngineDataStore.add(newCommitVersion);
                    // System.out.println("Insertion time : "
                    //         + (System.nanoTime() - start) / 1000 + " us");
                    log.info("cqengine : flow not found, first commit object");
                }
                else {
                    start = System.nanoTime();
                    for(VersionData entry : allCommits) {
                        allMatchingCommits.add(entry);
                    }
                    orderedCommits = sortDataStoreCommits(allMatchingCommits);
                    VersionData activeVersion = orderedCommits.lastElement();
                    log.info("Ordered commit list : " + orderedCommits);
                    log.info("Most recent version : " +
                           activeVersion.getVersion());
                    if(activeVersion.equals(newCommitVersion)) {
                        log.info("cqenginestore : version data has not changed, " +
                                "not committing");
                        return false;
                    } else {
                        log.info("cqenginestore : committing new version");
                        newCommitVersion.setVersion(orderedCommits.size() + 1);
                        newCommitVersion.setIsActiveVersion(true);
                        activeVersion.setIsActiveVersion(false);
                        newCommitVersion.activateStateVar
                                (VersionData.VersionDataState.ACTIVE);
                        if(rule.isPermanent()) {
                            newCommitVersion.activateStateVar
                                    (VersionData.VersionDataState.PERMANENT);
                        } else {
                            log.info("Temporary rule, not setting flag");
                        }
                        activeVersion.deactivateStateVar
                                (VersionData.VersionDataState.ACTIVE);
                        long insertStart = System.nanoTime();
                        cqEngineDataStore.add(newCommitVersion);
                        // System.out.println("Insertion time : "
                        //        + (System.nanoTime() - insertStart) / 1000 + " us");
                    }
                    // System.out.println("Object Addition Time : "
                    //        + (System.nanoTime() - start)/1000 + " us");
                }
                break;

            default:
                log.error("Unknown data store identifier, cannot locate data store");
                return false;
        }
        return true;
    }

    /**
     * Internal method to transform a byte array into a binary sequence
     * @param byteArray input byte array to convert to a binary string
     * @return binary sequence from byte array
     */
	private String createBinaryPrefixSequence(byte[] byteArray) {
        StringBuilder binaryString = new StringBuilder();

        for (byte b : byteArray) {
            int val = b;
            for (int i = 0; i < 8; i++)
            {
                binaryString.append((val & 128) == 0 ? 0 : 1);
                val <<= 1;
            }
        }
        log.info("Binary String : " + binaryString);
        return binaryString.toString();
    }

    /**
     * Internal method to compute the SHA1 hash for a given object
     **/
    private byte[] computeHash(String text)
            throws NoSuchAlgorithmException, UnsupportedEncodingException {

        MessageDigest md;
        try {
            md = MessageDigest.getInstance("SHA-1");
            byte[] object_hash = new byte[40];
            md.update(text.getBytes("UTF-8"), 0, text.length());
            object_hash = md.digest();
            return (object_hash);
        } catch (UnsupportedEncodingException e) {
            log.error("Unknown encoding scheme");
            throw e;
        } catch (NoSuchAlgorithmException e) {
            log.error("Unsupported hashing algorithm");
            throw e;
        }
    }

    /**
     * Internal method to compute the delta between two version objects
     * @param existingCommit commit from datastore
     * @param newCommit newly prepared commit version
     */
    private StringBuilder computeDelta(VersionData existingCommit,
                                       VersionData newCommit) {

        // If the entity is null or has no ID, it hasn't been persisted before,
        // so there's no delta to calculate
        if (newCommit == null) {
            return null;
        }
        Field[] fields = ArrayUtils.addAll(newCommit.getClass().getDeclaredFields(),
                VersionData.class.getDeclaredFields());
        Object oldField = null;
        Object newField = null;
        StringBuilder delta = new StringBuilder();
        for (Field field : fields) {
            field.setAccessible(true); // We need to access private fields
            try {
                oldField = field.get(existingCommit);
                newField = field.get(newCommit);
            } catch (IllegalArgumentException e) {
                log.error("Bad argument given");
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                log.error("Could not access the argument");
                e.printStackTrace();
            }
            if ((oldField != newField)
                    && (((oldField != null) &&
                    !oldField.equals(newField)) ||
                    ((newField != null) &&
                            !newField.equals(oldField)))) {
                delta.append(field.getName()).append(": [").append(oldField).append("] -> [")
                        .append(newField).append("]  ");
            }
        }
        return delta;
    }
	/**
	 * Internal flow rule event listener.
	 */
	private class VCFlowRuleListener implements FlowRuleListener {

		@Override
		public void event(FlowRuleEvent event) {

            FlowRule rule = event.subject();

            switch (event.type()) {
                case RULE_ADDED:
                    log.info("Added rule " + rule.id().value());
                    writeToDataStore("TABLE", "SINGLE-ADD", rule);
                    writeToDataStore("CQENGINE", "SINGLE-ADD", rule);
                    break;
				case RULE_UPDATED:
					break;
				case RULE_ADD_REQUESTED:
					break;
				case RULE_REMOVE_REQUESTED:
					break;
				case RULE_REMOVED:
                    log.info("Removed rule " + rule.id().value());
                    writeToDataStore("TABLE", "SINGLE-DEL", rule);
                    writeToDataStore("CQENGINE", "SINGLE-DEL", rule);
                    break;
				default:
					log.warn("Unknown flow rule event {}", event);
			}
		}
	}
}

